package com.swapp.models;

public class Videos {

    private int Id;

    private String Title;

    private String Type;

    private String Genre;

    private int MaximumAge;

    private int YearReleased;

    private String LoggedInUser;
    
    public Videos(int id, String title, String type, String genre) {
        Id = id;
        Title = title;
        Type = type;
        Genre = genre;
    }

    public String getLoggedInUser() {
        return LoggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.LoggedInUser = loggedInUser;
    }

    public int getId() {
        return Id;
    }

    public int getYearReleased() {
        return YearReleased;
    }

    public void setYearReleased(int yearReleased) {
        this.YearReleased = yearReleased;
    }

    public int getMaximumAge() {
        return MaximumAge;
    }

    public void setMaximumAge(int maximumAge) {
        this.MaximumAge = maximumAge;
    }

    public String getGenre() {
        return Genre;
    }

    public String getType() {
        return Type;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }
    
}
