package com.swapp.models;

public class Users {
    private String UserName;

    private String Password;

    public Users(String userName, String password) {
        setUserName(userName);
        setPassword(password);
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        this.UserName = userName;
    }
}
