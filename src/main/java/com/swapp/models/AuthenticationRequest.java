package com.swapp.models;

public class AuthenticationRequest {
    
    private String Name;
    private String Password;
    
    public AuthenticationRequest(){
    }

    public AuthenticationRequest(String name, String password) {
        setName(name);
        setPassword(password);
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }
}
