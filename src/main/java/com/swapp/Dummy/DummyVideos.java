package com.swapp.Dummy;

import java.util.ArrayList;
import java.util.List;

import com.swapp.models.Videos;

public class DummyVideos {

    public List<Videos> VideoList(){
        List<Videos> videos = new ArrayList<Videos>();
        videos.add(new Videos(1, "Just Us", "Regular", "Action"));
        videos.add(new Videos(2, "Twins", "ChildrenMovie", "Drama"));
        videos.add(new Videos(3, "Commanding", "NewRelease", "Romance"));
        videos.add(new Videos(4, "The Return", "Regular", "Comedy"));
        videos.add(new Videos(5, "No Exit", "Regular", "Horror"));
        return videos;
    }
}
