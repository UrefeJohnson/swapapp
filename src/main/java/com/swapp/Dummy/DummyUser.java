package com.swapp.Dummy;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.swapp.models.Users;

public class DummyUser {
   
    private List<Users> UserList(){
        List<Users> users = new ArrayList<Users>();
        users.add(new Users("Robert", "robert"));
        users.add(new Users("Ralph", "ralph"));
        users.add(new Users("Newton", "newton"));
        users.add(new Users("Seun", "seun"));
        users.add(new Users("Solo", "solo"));
        return users;
    }

    public Optional<Users> GetUser(String userName, String password){
        Optional<Users> user = UserList().stream()
        .filter(p -> p.getUserName() == userName && p.getPassword() == password).findFirst();        
        return user;
    }
}
