package com.swapp.swaapp;

import com.swapp.Services.VideoService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VideosController {

    @GetMapping("/videolist")
    public ResponseEntity<?> GetVideos() throws Exception{
        
        VideoService videoService = new VideoService();

        try{
            var videos = videoService.GetVideoList();         
            return ResponseEntity.ok(videos);
        }
        catch(Exception  e){
            String exceptionMsg = "unable to fulfill this request, please try again later!";
            return ResponseEntity.ok(exceptionMsg);
        }
    }    
}
