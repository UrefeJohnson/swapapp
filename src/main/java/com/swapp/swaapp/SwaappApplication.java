package com.swapp.swaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaappApplication.class, args);
	}

}
