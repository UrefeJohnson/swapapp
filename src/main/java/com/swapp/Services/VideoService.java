package com.swapp.Services;

import java.util.List;
import java.util.stream.Collectors;

import com.swapp.Dummy.DummyVideos;
import com.swapp.Util.VideoType;
import com.swapp.models.Users;
import com.swapp.models.Videos;

public class VideoService {
    DummyVideos vid = new DummyVideos();
    Users users;

    public List<Videos> GetVideoList(){
        VideoType ChldrenMovie = VideoType.ChildrenMovie; 
        VideoType NewRelease = VideoType.NewRelease; 
        List<Videos> videos = (List<Videos>) vid.VideoList().stream().collect(Collectors.toList()); 
        for (Videos video : videos) {
            video.setLoggedInUser(users.getUserName());
            if(video.getType() == ChldrenMovie.toString()){
                video.setMaximumAge(17);
            }
            if(video.getType() == NewRelease.toString()){
                video.setYearReleased(2021);
            }
        }
        return videos;
    }
}
